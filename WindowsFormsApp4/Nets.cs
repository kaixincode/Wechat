﻿// ********************************************
// 作者：honggenxiang
// 时间：2018-08-09 12:39:50
// ********************************************

using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Security;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace WindowsFormsApp4
{
    public sealed class Nets
    {
        /// <summary>
        /// http request请求
        /// </summary>
        /// <param name="url">资源地址</param>
        /// <param name="postData">查询条件</param>
        /// <param name="method">Get or Post or other</param>
        /// <param name="headers"></param>
        /// <param name="contentType">获取或设置 Content-type HTTP 标头的值。</param>
        /// <param name="userAgent"></param>
        /// <param name="timeout">超时时间</param>
        /// <param name="readAndWriteTimeout">读取数据流超时时间</param>
        /// <param name="encoding">编码格式</param>
        /// <param name="cookie">是否需要cookie</param>
        public static string Request(string url, string postData, string method, Encoding encoding = null, Dictionary<string, string> headers = null, string contentType = null, string userAgent = null, int timeout = 5000, int readAndWriteTimeout = 5000, CookieContainer cookie = null)
        {
            //var headers = new Dictionary<string, string>
            //{
            //    ["X-Sign"] = Encrypt.MD5Base64String($"{{\"deviceSn\":\"12123131313\"}}{KEY}", Encoding.UTF8),
            //};
            if (encoding == null) { encoding = Encoding.UTF8; }

            if (url.ToLower().StartsWith("https"))
            {
                //强制远程证书验证通过
                ServicePointManager.ServerCertificateValidationCallback += RemoteCertificateValidate;
            }

            HttpWebRequest request = null;
            WebResponse webRsp = null;
            var containsBody = postData != null;
            byte[] data = containsBody ? encoding.GetBytes(postData) : null;
            try
            {
                request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = method.ToUpper();
                request.ContentType = contentType ?? "application/x-www-form-urlencoded";
                request.Timeout = timeout;
                request.ReadWriteTimeout = readAndWriteTimeout;

                if (!string.IsNullOrEmpty(userAgent)) request.UserAgent = userAgent;
                if (headers != null)
                {
                    foreach (var header in headers)
                    {
                        request.Headers.Add(header.Key, header.Value);
                    }
                }

                if (cookie != null)
                {
                    request.CookieContainer = cookie;
                }

                if (containsBody)
                {
                    request.ContentLength = data.Length;
                    using (var newStream = request.GetRequestStream())
                    {
                        newStream.Write(data, 0, data.Length);
                    }
                }

                webRsp = request.GetResponse();
                using (var response = (HttpWebResponse)webRsp)
                {
                    string readToEnd;
                    if (cookie != null)
                    {
                        response.Cookies = cookie.GetCookies(request.RequestUri);
                    }

                    using (var stream = response.GetResponseStream())
                    {
                        using (var reader = new StreamReader(stream ?? throw new InvalidOperationException(), encoding))
                        {
                            readToEnd = reader.ReadToEnd();
                        }
                    }

                    return readToEnd;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                webRsp?.Close();
                request?.Abort();
            }

        }
        /// <summary>
        /// http request请求
        /// </summary>
        /// <param name="url">资源地址</param>
        /// <param name="postData">查询条件</param>
        /// <param name="method">Get or Post or other</param>
        /// <param name="headers"></param>
        /// <param name="contentType">获取或设置 Content-type HTTP 标头的值。</param>
        /// <param name="userAgent"></param>
        /// <param name="timeout">超时时间</param>
        /// <param name="readAndWriteTimeout">读取数据流超时时间</param>
        /// <param name="encoding">编码格式</param>
        /// <param name="cookie">是否需要cookie</param>
        public static Stream RequestStream(string url, string postData, string method, Encoding encoding = null, Dictionary<string, string> headers = null, string contentType = null, string userAgent = null, int timeout = 5000, int readAndWriteTimeout = 5000, CookieContainer cookie = null)
        {
            //var headers = new Dictionary<string, string>
            //{
            //    ["X-Sign"] = Encrypt.MD5Base64String($"{{\"deviceSn\":\"12123131313\"}}{KEY}", Encoding.UTF8),
            //};
            if (encoding == null) { encoding = Encoding.UTF8; }

            if (url.ToLower().StartsWith("https"))
            {
                //强制远程证书验证通过
                ServicePointManager.ServerCertificateValidationCallback += RemoteCertificateValidate;
            }

            HttpWebRequest request = null;
            WebResponse webRsp = null;
            var containsBody = postData != null;
            byte[] data = containsBody ? encoding.GetBytes(postData) : null;
            try
            {
                request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = method.ToUpper();
                request.ContentType = contentType ?? "application/x-www-form-urlencoded";
                request.Timeout = timeout;
                request.ReadWriteTimeout = readAndWriteTimeout;

                if (!string.IsNullOrEmpty(userAgent)) request.UserAgent = userAgent;
                if (headers != null)
                {
                    foreach (var header in headers)
                    {
                        request.Headers.Add(header.Key, header.Value);
                    }
                }

                if (cookie != null)
                {
                    request.CookieContainer = cookie;
                }

                if (containsBody)
                {
                    request.ContentLength = data.Length;
                    using (var newStream = request.GetRequestStream())
                    {
                        newStream.Write(data, 0, data.Length);
                    }
                }

                webRsp = request.GetResponse();
                using (var response = (HttpWebResponse)webRsp)
                {
                    if (cookie != null)
                    {
                        response.Cookies = cookie.GetCookies(request.RequestUri);
                    }
                    return response.GetResponseStream();
                    using (var stream = response.GetResponseStream())
                    {
                        return stream;
                    }


                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                webRsp?.Close();
                request?.Abort();
            }

        }
        /// <summary>
        /// 发送get请求
        /// </summary>
        /// <param name="url">请求地址</param>
        /// <returns>响应结果</returns>
        public static string Get(string url)
        {
            return Request(url, null, "GET");
        }


        /// <summary>  
        /// 远程证书验证，固定返回true 
        /// </summary>  
        private static bool RemoteCertificateValidate(object sender, X509Certificate cert, X509Chain chain, SslPolicyErrors error)
        {
            return true;
        }
        #region public static bool IsConnectedInternet() 检测本机是否联网（互联网）
        [DllImport("wininet")]
        //http://baike.baidu.com/view/560670.htm（这是百度对wininet的解释）
        private extern static bool InternetGetConnectedState(out int connectionDescription, int reservedValue);
        /// <summary>
        /// 检测本机是否联网
        /// </summary>
        /// <returns></returns>
        public static bool IsConnectedInternet()
        {
            try
            {
                int i;
                return InternetGetConnectedState(out i, 0);
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// 用于检查IP地址或域名是否可以使用TCP/IP协议访问(使用Ping命令),true表示Ping成功,false表示Ping失败 
        /// </summary>
        /// <param name="strIpOrDName">输入参数,表示IP地址或域名</param>
        /// <returns></returns>
        public static bool PingIpOrDomainName(string strIpOrDName)
        {
            try
            {
                Ping objPingSender = new Ping();
                PingOptions objPinOptions = new PingOptions();
                objPinOptions.DontFragment = true;
                string data = "";
                byte[] buffer = Encoding.UTF8.GetBytes(data);
                int intTimeout = 120;
                PingReply objPinReply = objPingSender.Send(strIpOrDName, intTimeout, buffer, objPinOptions);
                string strInfo = objPinReply.Status.ToString();
                if (strInfo == "Success")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion

    }
}