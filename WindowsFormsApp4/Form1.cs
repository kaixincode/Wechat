﻿using SufeiUtil;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp4.Util.Model;
using ZTO.CPF.Core.Util.Extends;

namespace WindowsFormsApp4
{
    public partial class Form1 : Form
    {
        private long timestamp;
        private Wechat wechat = new Wechat();
        private string cookie;
        public Form1()
        {
            InitializeComponent();
        }
        /// <summary>
        /// 获取微信uuid
        /// </summary>
        /// <returns></returns>
        private string LoadLoginQrCode()
        {
            //            https://login.web.wechat.com/jslogin?appid=wx782c26e4c19acffb&redirect_uri=https%3A%2F%2Fweb.wechat.com%2Fcgi-bin%2Fmmwebwx-bin%2Fwebwxnewloginpage&fun=new&lang=zh_CN&_=1559112784612
            //            window.QRLogin.code = 200; window.QRLogin.uuid = "oY4ccnfq7Q==";
            //            获取到uuid 后面获取登陆二维码用到
            //            https://login.weixin.qq.com/qrcode/uuid
            var pattern = "window.QRLogin.code = 200; window.QRLogin.uuid = \"(\\S+)\";";
            var result = Nets.Get("https://login.web.wechat.com/jslogin?appid=wx782c26e4c19acffb&redirect_uri=https%3A%2F%2Fweb.wechat.com%2Fcgi-bin%2Fmmwebwx-bin%2Fwebwxnewloginpage&fun=new&lang=zh_CN&_=1559112784612");
            var d = DateTime.Now;
            timestamp = d.ToMillisecondTimestamp();
            var isGet = Regex.IsMatch(result, pattern);
            if (!isGet) { MessageBox.Show("获取uuid失败"); return null; }
            var matchResult = Regex.Match(result, pattern);
            return matchResult.Groups[1].Value;
        }
        private string GetAvatar()
        {
            //            轮询刷新二维码获取头像
            //https://login.web.wechat.com/cgi-bin/mmwebwx-bin/login?loginicon=true&uuid=4fQOMyVcYA==&tip=1&r=-41108286&_=1559113965855
            //window.code = 201; window.userAvatar = 'base64'
            try
            {
                var pattern = "window.code=201;window.userAvatar = 'data:img/jpg;base64,(\\S+)';";
                var result = Nets.Request($"https://login.wx.qq.com/cgi-bin/mmwebwx-bin/login?loginicon=true&uuid={wechat.uuid}&tip=1&r=-41108286&_={timestamp}", null, "GET", timeout: 10 * 1000);
                var isGet = Regex.IsMatch(result, pattern);
                if (!isGet) { MessageBox.Show("获取头像失败"); return null; }
                var matchResult = Regex.Match(result, pattern);
                return matchResult.Groups[1].Value;
            }
            catch (Exception)
            {
                return null;

            }


        }
        private string GetTicket()
        {
            //调用获取登陆凭证
            //https://login.wx.qq.com/cgi-bin/mmwebwx-bin/login?loginicon=true&uuid=YclDizxCrQ==&tip=0&r=-47919316&_=1559121040122
            //window.redirect_uri="https://wx.qq.com/cgi-bin/mmwebwx-bin/webwxnewloginpage?ticket=AROiQLTSEsntDwWolEq7MRWb@qrticket_0&uuid=YclDizxCrQ==&lang=zh_CN&scan=1559121049";
            try
            {
                var pattern = "window.redirect_uri=\"(\\S+)\";";
                var result = Nets.Request($"https://login.wx.qq.com/cgi-bin/mmwebwx-bin/login?loginicon=true&uuid={wechat.uuid}&tip=0&r=-47919316&_={timestamp}", null, "GET", timeout: 10 * 1000);
                var isGet = Regex.IsMatch(result, pattern);
                if (!isGet) { MessageBox.Show("获取登陆凭证失败"); return null; }
                var matchResult = Regex.Match(result, pattern);
                pattern = "ticket=(\\S+0)&";
                wechat.ticket = Regex.Match(result, pattern).Groups[1].Value;
                return matchResult.Groups[1].Value + "&fun=new&version=v2&lang=zh_CN";
            }
            catch (Exception)
            {
                return null;

            }


        }
        private bool DoLogin(string loginUrl, out string cookie)
        {
            //调用获取登陆凭证
            //https://wx.qq.com/cgi-bin/mmwebwx-bin/webwxnewloginpage?ticket=AfxFafyDuE17bzvo0KiJtO2G@qrticket_0&uuid=gZaeExv42g==&lang=zh_CN&scan=1559122101&fun=new&version=v2&lang=zh_CN
            //<error><ret>0</ret><message></message><skey>@crypt_9886a8af_388afa46b6a4151852a02922b07f2fc1</skey><wxsid>DihXUOhZcml1GZNU</wxsid><wxuin>398022320</wxuin><pass_ticket>6Is1yp1wnq%2Fns2pE48rhGL6QaSyalDNRD6P6c3xUJzL67QWAcb0dI7e6IGa1o7DT</pass_ticket><isgrayscale>1</isgrayscale></error>
            //<error><ret>0</ret><message></message><skey>(\\S+)</skey><wxsid>(\\S+)</wxsid><wxuin>(\\S+)</wxuin><pass_ticket>(\\S+)</pass_ticket><isgrayscale>(\\S+)</isgrayscale></error>
            try
            {
                var pattern = "<error><ret>0</ret><message></message><skey>(\\S+)</skey><wxsid>(\\S+)</wxsid><wxuin>(\\S+)</wxuin><pass_ticket>(\\S+)</pass_ticket><isgrayscale>(\\S+)</isgrayscale></error>";
                HttpHelper http = new HttpHelper();
                HttpResult result = http.GetHtml(new HttpItem()
                {
                    URL = loginUrl,
                    Timeout = 10 * 100,
                });
                cookie = result.Cookie;
                var isGet = Regex.IsMatch(result.Html, pattern);
                if (!isGet) { MessageBox.Show("获取登陆sid失败"); return false; }
                var matchResult = Regex.Match(result.Html, pattern);
                wechat.skey = matchResult.Groups[1].Value;
                wechat.wxsid = matchResult.Groups[2].Value;
                wechat.wxuin = matchResult.Groups[3].Value;
                wechat.pass_ticket = matchResult.Groups[4].Value;
                wechat.isgrayscale = Convert.ToInt32(matchResult.Groups[5].Value);
                return true;
            }
            catch (Exception)
            {
                cookie = "";
                return false;
            }


        }
        private bool WebWxInit(string cookie)
        {
            //调用获取登陆凭证
            //https://wx.qq.com/cgi-bin/mmwebwx-bin/webwxinit?r=-198608425&pass_ticket=u5zS0wCs4QN6wqfn08i3AwQ8aB0xltV35P3z7QXaIn1OZo3BuyJP9aXxniI9zRZN
            //<error><ret>0</ret><message></message><skey>@crypt_9886a8af_388afa46b6a4151852a02922b07f2fc1</skey><wxsid>DihXUOhZcml1GZNU</wxsid><wxuin>398022320</wxuin><pass_ticket>6Is1yp1wnq%2Fns2pE48rhGL6QaSyalDNRD6P6c3xUJzL67QWAcb0dI7e6IGa1o7DT</pass_ticket><isgrayscale>1</isgrayscale></error>
            //<error><ret>0</ret><message></message><skey>(\\S+)</skey><wxsid>(\\S+)</wxsid><wxuin>(\\S+)</wxuin><pass_ticket>(\\S+)</pass_ticket><isgrayscale>(\\S+)</isgrayscale></error>
            try
            {
                var pattern = "<error><ret>0</ret><message></message><skey>(\\S+)</skey><wxsid>(\\S+)</wxsid><wxuin>(\\S+)</wxuin><pass_ticket>(\\S+)</pass_ticket><isgrayscale>(\\S+)</isgrayscale></error>";
                HttpHelper http = new HttpHelper();
                String DeviceID = "e" + (new Random().NextDouble()).ToString().Substring(1, 16);
                var postData = $"{{\"BaseRequest\":{{\"Uin\":\"{wechat.wxuin}\",\"Sid\":\"{wechat.wxsid}\",\"Skey\":\"{wechat.skey}\",\"DeviceID\":\"{DeviceID}\"}}}}";
                HttpResult result = http.GetHtml(new HttpItem()
                {
                    URL = $"https://wx.qq.com/cgi-bin/mmwebwx-bin/webwxinit?r=-198608425&pass_ticket={wechat.pass_ticket}",
                    Method = "POST",
                    Cookie = cookie,
                    ContentType = "application/json;charset=UTF-8",
                    Postdata = postData
                });
                var isGet = Regex.IsMatch(result.Html, pattern);
                if (!isGet) { MessageBox.Show("获取登陆sid失败"); return false; }
                var matchResult = Regex.Match(result.Html, pattern);
                wechat.skey = matchResult.Groups[1].Value;
                wechat.wxsid = matchResult.Groups[2].Value;
                wechat.wxuin = matchResult.Groups[3].Value;
                wechat.pass_ticket = matchResult.Groups[4].Value;
                wechat.isgrayscale = Convert.ToInt32(matchResult.Groups[5].Value);
                return true;
            }
            catch (Exception)
            {
                return false;
            }


        }
        private void btn_LoadQrCode_Click(object sender, EventArgs e)
        {

            Task.Factory.StartNew(() =>
            {
                while (true)
                {
                    wechat.uuid = LoadLoginQrCode();
                    btn_LoadQrCode.Invoke((Action)(() =>
                    {
                        btn_LoadQrCode.Enabled = false;
                    }));
                    pic_qrcode.ImageLocation = $"https://login.weixin.qq.com/qrcode/{wechat.uuid}";
                    var avatar = GetAvatar();
                    if (avatar == null)
                    {
                        continue;
                    }
                    else
                    {
                        pic_qrcode.Image = Base64ToImage(avatar);
                        var loginUrl = GetTicket();
                        if (loginUrl == null)
                        {
                            continue;
                        }
                        if (!DoLogin(loginUrl, out cookie))
                        {

                            continue;
                        }
                        btn_Logout.Invoke((Action)(() =>
                        {
                            btn_Logout.Enabled = true;
                        }));

                        //MessageBox.Show(wechat.ticket);

                        break;
                    }
                }
                WebWxInit(cookie);
            });

        }


        public System.Drawing.Image Base64ToImage(string base64String)
        {
            byte[] imageBytes = Convert.FromBase64String(base64String);
            MemoryStream ms = new MemoryStream(imageBytes, 0, imageBytes.Length);
            ms.Write(imageBytes, 0, imageBytes.Length);
            System.Drawing.Image image = System.Drawing.Image.FromStream(ms, true);
            return image;
        }

        private void btn_Logout_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
