﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp4.Util.Model
{
    public class Wechat
    {
        public string uuid { get; set; }
        public string ticket { get; set; }
        public string skey { get; set; }
        public string wxsid { get; set; }
        public string wxuin { get; set; }
        public string pass_ticket { get; set; }
        public int isgrayscale { get; set; }

    }
}
