﻿namespace WindowsFormsApp4
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btn_Logout = new System.Windows.Forms.Button();
            this.btn_LoadQrCode = new System.Windows.Forms.Button();
            this.pic_qrcode = new System.Windows.Forms.PictureBox();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic_qrcode)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btn_Logout);
            this.groupBox1.Controls.Add(this.btn_LoadQrCode);
            this.groupBox1.Controls.Add(this.pic_qrcode);
            this.groupBox1.Location = new System.Drawing.Point(24, 25);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(426, 339);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "微信设置";
            // 
            // btn_Logout
            // 
            this.btn_Logout.Enabled = false;
            this.btn_Logout.Location = new System.Drawing.Point(323, 33);
            this.btn_Logout.Name = "btn_Logout";
            this.btn_Logout.Size = new System.Drawing.Size(75, 23);
            this.btn_Logout.TabIndex = 2;
            this.btn_Logout.Text = "下线";
            this.btn_Logout.UseVisualStyleBackColor = true;
            this.btn_Logout.Click += new System.EventHandler(this.btn_Logout_Click);
            // 
            // btn_LoadQrCode
            // 
            this.btn_LoadQrCode.Location = new System.Drawing.Point(217, 33);
            this.btn_LoadQrCode.Name = "btn_LoadQrCode";
            this.btn_LoadQrCode.Size = new System.Drawing.Size(75, 23);
            this.btn_LoadQrCode.TabIndex = 1;
            this.btn_LoadQrCode.Text = "加载二维码";
            this.btn_LoadQrCode.UseVisualStyleBackColor = true;
            this.btn_LoadQrCode.Click += new System.EventHandler(this.btn_LoadQrCode_Click);
            // 
            // pic_qrcode
            // 
            this.pic_qrcode.Location = new System.Drawing.Point(16, 33);
            this.pic_qrcode.Name = "pic_qrcode";
            this.pic_qrcode.Size = new System.Drawing.Size(180, 190);
            this.pic_qrcode.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_qrcode.TabIndex = 0;
            this.pic_qrcode.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(471, 386);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pic_qrcode)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btn_LoadQrCode;
        private System.Windows.Forms.PictureBox pic_qrcode;
        private System.Windows.Forms.Button btn_Logout;
    }
}

